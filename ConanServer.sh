#!/bin/bash

baseDir=$( pwd )

tmuxStart() {
   tmux ls | grep "$1" && { return 0; }
   tmux new-session -d -A -s $1
}

ConanServerSteam() {
   steamcmd +@sSteamCmdForcePlatformType windows +login anonymous +force_install_dir "$1"/"$2" +app_update 443030 +exit
}

downloadMods() {
##
## Read modlist.csv and download mods
## Also create modlist.txt using modlist.csv and downloaded PAK file name
##
if [ ! -d "$1"/"$2"/ConanSandbox/Mods ] ; then
   mkdir -p "$1"/"$2"/ConanSandbox/Mods
fi
if [ -f "$1"/"$2"/ConanSandbox/Mods/modlist.txt ] ; then
   rm "$1"/"$2"/ConanSandbox/Mods/modlist.txt
fi
while IFS=, read -r modName modID
do
   echo "Downloading mod: $modName"
   steamcmd +@sSteamCmdForcePlatformType windows +login anonymous +force_install_dir "$1"/"$2" +workshop_download_item 440900 "$modID" +exit
   modName=$(ls "$1"/"$2"/steamapps/workshop/content/440900/"$modID")
   echo \*"$1"/"$2"/steamapps/workshop/content/440900/"$modID"/"$modName" >> "$1"/"$2"/ConanSandbox/Mods/modlist.txt
done < "$baseDir"/modlist.csv
unix2dos "$1"/"$2"/ConanSandbox/Mods/modlist.txt

echo ""
}

# Checks for the existance of a config.csv file.
# If one is not found, assume first run and prompt for subdir to create server in
# If found read value for server sub dir and check for updates with steamcmd.

if [ ! -f "$baseDir"/config.csv ] ; then
   echo "No Config.csv found. Performing first time setup." 
   echo "Please subdirectory for serer install"
   read serverDir
   echo Server Directory,"$serverDir" > config.csv
   mkdir "$serverDir"
   echo ""
   echo "installing Conan Exiles Server using steamcmd."
   echo ""
   ConanServerSteam "$baseDir" "$serverDir"
else
   while IFS=, read -r setting value
   do
      if [ "$setting" = "Server Directory" ] ; then
         serverDir="$value"
      fi
   echo "Updating Server."
      ConanServerSteam "$baseDir" "$serverDir"
   done < config.csv
fi

## Next we check for the existance of a "mostlist.csv" file.
## If Found, download the mods from the csv.
## Format for the CSV file <mod name>,<mod id>
##      example: testing mod,9999999999

if [ -f "$baseDir"/modlist.csv ] ; then
   echo "modlist.csv Found.  Downloading/updating mods."
   echo ""
   downloadMods "$baseDir" "$serverDir"
else
   echo "No modlist.csv file found.  No mods configured for installation."
fi

tmuxStart Conan1
tmux send-keys -t Conan1.0 "cd $baseDir" ENTER
tmux send-keys -t Conan1.0 "cd $serverDir" ENTER
tmux send-keys -t Conan1.0 "xvfb-run --auto-servernum --server-args='-screen 0 640x480x24:32' wine ConanSandboxServer.exe -log" ENTER
echo "Conan Exiles Server start on tmux session Conan1."
